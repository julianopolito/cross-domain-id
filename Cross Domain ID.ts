export class RedirectTemplate implements CampaignTemplateComponent {
    @title("Auth URL (Cross Domain)")
    @subtitle("Page url for capturing ID in another domain")
    authURL:String = "";
    
    @title("ID Param")
    @subtitle("Param to be added to Auth URL with ID")
    idParam:String = "id";

    @title("Use Attribute as ID")
    @subtitle("If set to false, user.id will be used")
    useIdAttribute:Boolean = false;

    @title("Attribute Name")
    attributeName:string = "emailAddress";

    private stripParams(url: string): string {
        return url.replace(/(\/)?(\?.*)?$/, "");
    }
    private updateQueryStringParameter(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }else {
            return uri + separator + key + "=" + value;
        }
    }
    run(context: CampaignComponentContext) {
        var tstprofile:string = context?.user.id.toString();       
        if(this.useIdAttribute){
            tstprofile = context?.user?.attributes[this.attributeName]?.value.toString();
        }
        return {
            pid: tstprofile,
            att:context?.user?.attributes,
            targetURL: this.updateQueryStringParameter(this.authURL, this.idParam, tstprofile)
        };
    }

}
